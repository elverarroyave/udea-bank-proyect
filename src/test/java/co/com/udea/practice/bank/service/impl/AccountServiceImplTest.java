package co.com.udea.practice.bank.service.impl;

import co.com.udea.practice.bank.model.entity.Account;
import co.com.udea.practice.bank.model.entity.Bank;
import co.com.udea.practice.bank.model.entity.User;
import co.com.udea.practice.bank.repository.AccountRepository;
import co.com.udea.practice.bank.repository.BankRepository;
import co.com.udea.practice.bank.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.BDDMockito.given;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    @InjectMocks
    private AccountServiceImpl accountService;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private BankRepository bankRepository;
    @Mock
    private UserRepository userRepository;

    User user;
    Bank bank;
    Account account;


    @BeforeEach
    void setUp(){
        user = new User(1L, "Pepito", "1234", 22);
        bank = new Bank(1L, "MYBANCO", 0);
        account = new Account(BigDecimal.ZERO,user,bank);
    }

    @Test
    void should_create_success_account() {
        //given
        given(userRepository.findById(1L)).willReturn(Optional.of(user));
        given(bankRepository.findById(1L)).willReturn(Optional.of(bank));
        given(accountRepository.save(account)).willReturn(account);
        //when
        Account accountCreated = accountService.create(1L,1L);
        //then
        assertThat(accountCreated).isNotNull();

    }

    @Test
    void should_findById_successfully(){
        //given
        given(accountRepository.findById(anyLong())).willReturn(Optional.of(account));
        //when
        Account accountFound = accountService.findById(1L);
        //then
        assertThat(accountFound).isNotNull();
    }
}