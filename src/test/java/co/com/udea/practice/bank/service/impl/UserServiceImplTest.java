package co.com.udea.practice.bank.service.impl;

import co.com.udea.practice.bank.model.entity.User;
import co.com.udea.practice.bank.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;


    private User user;

    @BeforeEach
    void setUp() {
        user = new User(1L, "Pepito", "1234", 22);
    }

    @Test
    void should_create_success_user(){
        given(userRepository.save(user)).willReturn(user);
        User userCreated = userService.create(user);
        assertThat(userCreated).isNotNull();
    }
}