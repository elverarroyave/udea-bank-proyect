package co.com.udea.practice.bank.service;

import co.com.udea.practice.bank.model.entity.User;

public interface UserService {
    User create(User user);
}
