package co.com.udea.practice.bank.service;

import co.com.udea.practice.bank.model.entity.Bank;

public interface BankService {
    Bank create(String name);
}
