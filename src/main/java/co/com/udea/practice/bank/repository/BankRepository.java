package co.com.udea.practice.bank.repository;

import co.com.udea.practice.bank.model.entity.Bank;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankRepository extends JpaRepository<Bank, Long> {
}
