package co.com.udea.practice.bank.controller.v1;

import co.com.udea.practice.bank.model.entity.Account;
import co.com.udea.practice.bank.service.AccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

import static org.springframework.web.util.UriComponentsBuilder.fromUriString;

@RestController
@RequestMapping("api/v1/account/")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService){
        this.accountService = accountService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Account> findById(@PathVariable("id") Long id){
        return ResponseEntity.ok(accountService.findById(id));
    }

    @PostMapping
    public ResponseEntity<Void> create(@RequestParam("bankId") Long bankId, @RequestParam("userId") Long idUser){
        Account account = accountService.create(bankId, idUser);
        URI location = fromUriString("/api/v1/account").path("/{id}")
                .buildAndExpand(account.getId()).toUri();
        return ResponseEntity.created(location).build();
    }
}
