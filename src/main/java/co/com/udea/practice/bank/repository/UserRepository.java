package co.com.udea.practice.bank.repository;

import co.com.udea.practice.bank.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
