package co.com.udea.practice.bank.controller.v1;

import co.com.udea.practice.bank.model.entity.Bank;
import co.com.udea.practice.bank.service.BankService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

import static org.springframework.web.util.UriComponentsBuilder.fromUriString;

@RestController
@RequestMapping("api/v1/bank")
public class BankController {

    private final BankService bankService;

    public BankController(BankService bankService){
        this.bankService = bankService;
    }

    @PostMapping
    public ResponseEntity<Void> create(@RequestParam String name){
        Bank bank = bankService.create(name);
        URI location = fromUriString("/api/v1/bank").path("/{id}")
                .buildAndExpand(bank.getId()).toUri();
        return ResponseEntity.created(location).build();
    }
}
