package co.com.udea.practice.bank.repository;

import co.com.udea.practice.bank.model.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {
}
