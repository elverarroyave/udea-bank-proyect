package co.com.udea.practice.bank.service;

import co.com.udea.practice.bank.model.entity.Account;

import java.math.BigDecimal;

public interface AccountService {
    Account create(Long  bankId, Long userId);
    Account findById(Long id);
    BigDecimal checkBalance(Long accountId);
    void transfer(Long originAccountId, Long destinationAccountId, BigDecimal quantity);
}
