package co.com.udea.practice.bank.service.impl;

import co.com.udea.practice.bank.model.entity.Bank;
import co.com.udea.practice.bank.repository.BankRepository;
import co.com.udea.practice.bank.service.BankService;
import org.springframework.stereotype.Service;

@Service
public class BankServiceImpl implements BankService {

    private final BankRepository bankRepository;

    public BankServiceImpl(BankRepository bankRepository){
        this.bankRepository = bankRepository;
    }

    @Override
    public Bank create(String name) {
        Bank newBank = Bank.builder()
                .name(name)
                .totalTransfers(0)
                .build();
        return bankRepository.save(newBank);
    }
}
