package co.com.udea.practice.bank.model.entity;

import co.com.udea.practice.bank.config.exceptions.InsufficientCreditException;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;

@Data
@Builder(toBuilder = true)
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private BigDecimal credit;
    @OneToOne
    @JoinColumn
    private User user;
    @OneToOne
    @JoinColumn
    private Bank bank;

    public Account(BigDecimal credit, User user, Bank bank){
        this.credit = credit;
        this.user = user;
        this.bank = bank;
    }

    public void debit(BigDecimal amount){
        BigDecimal newCredit = this.credit.subtract(amount);
        if(newCredit.compareTo(BigDecimal.ZERO) < 0)
            throw new InsufficientCreditException("Insufficient credit in your account.");
        this.credit = newCredit;
    }

    public void deposit(BigDecimal amount){
        this.credit = this.credit.add(amount);
    }
}
