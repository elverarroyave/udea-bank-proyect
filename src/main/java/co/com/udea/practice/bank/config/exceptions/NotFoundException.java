package co.com.udea.practice.bank.config.exceptions;

public class NotFoundException extends RuntimeException{
    public NotFoundException() {
    }
    public NotFoundException(String message) {
        super(message);
    }
}