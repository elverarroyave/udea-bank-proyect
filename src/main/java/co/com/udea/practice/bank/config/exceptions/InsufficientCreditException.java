package co.com.udea.practice.bank.config.exceptions;

public class InsufficientCreditException extends RuntimeException{
    public InsufficientCreditException(String mensaje) {
        super(mensaje);
    }
}
