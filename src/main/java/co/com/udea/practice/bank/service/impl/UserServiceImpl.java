package co.com.udea.practice.bank.service.impl;

import co.com.udea.practice.bank.model.entity.User;
import co.com.udea.practice.bank.repository.UserRepository;
import co.com.udea.practice.bank.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository){
        this.userRepository = userRepository;
    }


    @Override
    public User create(User user) {
        return userRepository.save(user);
    }
}
