package co.com.udea.practice.bank.model.entity;

import jakarta.persistence.*;
import lombok.*;

@Data
@Builder(toBuilder = true)
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Bank {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private int totalTransfers;
}
