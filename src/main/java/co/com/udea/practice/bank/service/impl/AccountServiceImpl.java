package co.com.udea.practice.bank.service.impl;

import co.com.udea.practice.bank.config.exceptions.NotFoundException;
import co.com.udea.practice.bank.model.entity.Account;
import co.com.udea.practice.bank.model.entity.Bank;
import co.com.udea.practice.bank.model.entity.User;
import co.com.udea.practice.bank.repository.AccountRepository;
import co.com.udea.practice.bank.repository.BankRepository;
import co.com.udea.practice.bank.repository.UserRepository;
import co.com.udea.practice.bank.service.AccountService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    private final BankRepository bankRepository;

    private final UserRepository userRepository;

    public AccountServiceImpl(AccountRepository accountRepository, BankRepository bankRepository, UserRepository userRepository){
        this.accountRepository = accountRepository;
        this.bankRepository = bankRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Account create(Long bankId, Long userId) {
        User userFound = userRepository.findById(userId).orElseThrow(
                ()-> new NotFoundException("User not found.")
        );
        Bank bankFound = bankRepository.findById(bankId).orElseThrow(
                ()-> new NotFoundException("Bank not found")
        );
        Account newAccount = new Account(BigDecimal.ZERO, userFound, bankFound);
        return accountRepository.save(newAccount);
    }

    @Override
    public Account findById(Long id) {
        return accountRepository.findById(id).orElseThrow(()->
        new NotFoundException("Account not found."));
    }
    @Override
    public BigDecimal checkBalance(Long accountId) {
        return findById(accountId).getCredit();
    }
    @Override
    public void transfer(Long originAccountId, Long destinationAccountId, BigDecimal quantity) {
        Account originAccount = findById(originAccountId);
        Account destinationAccount = findById(destinationAccountId);
        originAccount.debit(quantity);
        destinationAccount.deposit(quantity);
    }
}
