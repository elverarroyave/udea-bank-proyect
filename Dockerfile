# Usa una imagen base de OpenJDK para Java 17
FROM openjdk:17-jdk-alpine

# Establece el directorio de trabajo en /app
WORKDIR /app
RUN pwd
RUN ls -la
# Copia el archivo JAR construido por Gradle a la imagen
COPY build/libs/bank-0.0.1-SNAPSHOT.jar /app/app.jar

# Expone el puerto 8080 en la imagen
EXPOSE 8080

# Comando para ejecutar la aplicación Spring Boot cuando el contenedor se inicie
CMD ["java", "-jar", "app.jar"]
